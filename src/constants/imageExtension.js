export const IMAGE_EXTENSION = {
    JPEG: 'jpeg',
    PNG: 'png',
    AVIF: 'avif',
    WEBP: 'webp',
}
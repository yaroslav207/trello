export const IMAGE_EXTENSION = {
    JPEG: 'image/jpeg',
    PNG: 'image/png',
    AVIF: 'image/avif',
    WEBP: 'image/webp',
}
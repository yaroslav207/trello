export const MAX_SCALE = 3;
export const MIN_SCALE = 0.2;
export const DEFAULT_SCALE = 1;
export const STEP_SCALE_CHANGE = 0.2;

export const MAX_QUALITY_IMAGE = 100;
export const MIN_QUALITY_IMAGE = 1;

export const MIN_BLUR = 0;
export const MAX_BLUR = 20;
export const DEFAULT_BLUR = 0;

export const MIN_BRIGHTNESS = 1;
export const MAX_BRIGHTNESS = 200;
export const DEFAULT_BRIGHTNESS = 100;

export const MIN_SEPIA = 0;
export const MAX_SEPIA = 100;
export const DEFAULT_SEPIA = 0;

export const DEFAULT_OFFSETS = {
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
};
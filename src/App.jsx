import React, {useState} from 'react'
import ImagesView from './components/ImagesView';
import Menu from './components/Menu';
import {
    DEFAULT_SCALE,
    MAX_QUALITY_IMAGE,
    DEFAULT_SEPIA,
    DEFAULT_BLUR,
    DEFAULT_BRIGHTNESS, DEFAULT_OFFSETS
} from './base/constants/app';
import Dropzone from './components/Dropzone';
import {IMAGE_EXTENSION} from "./constants/imageExtension";
import {loadImage} from "./helpers/loadImage";

function App() {
    const [image, setImage] = useState(null);
    const [configurationImage, setConfigurationImage] = useState({
        scale: DEFAULT_SCALE,
        quality: MAX_QUALITY_IMAGE,
        blur: DEFAULT_BLUR,
        brightness: DEFAULT_BRIGHTNESS,
        sepia: DEFAULT_SEPIA,
        extension: IMAGE_EXTENSION.PNG,
        ...DEFAULT_OFFSETS,
    });

    const handleChangeConfigurationImage = (newValue) => {
        setConfigurationImage(newValue);
    };

    const handleLoadImage = loadImage(setImage);

    return (
        <div className='app'>
            {image
                ? <>
                    <ImagesView image={image} configurationImage={configurationImage}/>
                    <Menu
                        onChangeConfig={handleChangeConfigurationImage}
                        configurationValue={configurationImage}
                    />
                </>
                : <Dropzone onChange={handleLoadImage} className="dropzone"/>
            }
        </div>
    );
}

export default App;

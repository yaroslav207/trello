import React, {useEffect} from 'react';
import {useDropzone} from 'react-dropzone';
import {ReactComponent as IconUpload} from '../assets/images/upload.svg';

function Dropzone({onChange}) {
    const {getRootProps, getInputProps, acceptedFiles} = useDropzone({noDrag: true});

    useEffect(() => {
        if (acceptedFiles.length) {
            onChange(acceptedFiles)
        }
    }, [acceptedFiles])

    return (
        <section className="container">
            <div {...getRootProps({className: 'dropzone'})}>
                <input {...getInputProps()} />
                <p>Dropzone with no drag events</p>
                <IconUpload />
            </div>
        </section>
    );
}


export default Dropzone;
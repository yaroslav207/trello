import React, {useRef, useEffect, useCallback} from 'react';
import {canvasService} from '../services/canvasService';
import {debounce} from "../helpers/debounce";

const ImagesView = ({image, configurationImage}) => {
    const canvasRef = useRef();

    const changeConfigWithDebounce = useCallback(
        debounce(canvasService.changeConfig.bind(canvasService), 300),
        [image]);

    useEffect(() => {
        changeConfigWithDebounce(configurationImage)
    }, [configurationImage]);

    useEffect(() => {
        if (image) {
            canvasService.init(image, canvasRef.current, configurationImage)
        }
    }, [image]);

    return <div className='image-view'>
        <div>
            <img src={image.src}/>
        </div>
        <div>
            <canvas ref={canvasRef}/>
        </div>
    </div>
}

export default ImagesView;